var typeChecker = require('./common.types.js');

module.export = function(def){
    /*
     * Form-like processing
     *
     * Usage:
     *  > form_definition = {
     *  >   'title': {type: 'string', maxLength: 30, charset: 'base32'},
     *  >   ...
     *  > }
     *  > form = require('./common.form.js')(form_definition);
     *  > form.val('title', 'AXSZ3201...');
     */
    storageInit = {}

    for(keyName in def){
        fieldDef = def[keyName];
        if(['base32','base64','integer','arrayKV'].indexOf(fieldDef['type']) < 0)
            throw new Error('Unsupported form type.');
        storageInit[keyName] = ((fieldDef['type'] === 'arrayKV')?{}:null);
    }

    return {
        _storage: storageInit,

        val: function(keyName, v1, v2){
            if(def[keyName] == undefined)
                throw new Error('Unrecognized form field.');

            fieldDef = def[keyName];
            if(fieldDef['type'] === 'arrayKV'){
                if(v1 === undefined)
                    return this._storage[keyName];
                else if(v2 == undefined){
                    if(this._storage[keyName][v1] === undefined)
                        throw new Error('Form value not yet set: [' + keyName + '][' + v1 + '].');
                    return this._storage[keyName][v1];
                } else {
                    if(typeChecker.arrayKVItem(v2))
                        this._storage[keyName][v1] = v2;
                    else
                        throw new Error('Attempt to assign invalid value.');
                    return this;
                }
            } else {
                
            }
        },
    };
}
