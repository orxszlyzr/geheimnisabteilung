var IDENTITY = {
    _storage: {
        'title': '',
        'description': '',
        'kv': '',
    },

    _types: require('./common.types.js'),

    val: function(name, v1, v2){
        /*
         * Set/get fields in IDENTITY
         *
         * Usage:
         *  1) IDENTITY.val('title'); // gets 'title'
         *  2) IDENTITY.val('description', 'SOME DESCRIPTION'); // sets 'description'
         *  3) IDENTITY.val('kv'); // gets all key-value attachments
         *  4) IDENTITY.val('kv', 'KEY', 'SAMPLE'); // sets 'KEY'=>'SAMPLE'
         *  5) IDENTITY.val('kv', 'KEY'); // gets a 'SAMPLE'
         *  6) IDENTITY.val('kv', {'KEY1': 'VALUE1', 'KEY2: 'VALUE2'}); // sets 2 key-value pairs.
         */
        validType = true;

        if(['title','description'].indexOf(name) >= 0){
            if(v1 == undefined)
                return this._storage[name];
            else {
                if(this._types.is
                this._storage[name] = v1;
            }
        } else if(name == 'kv'){
            if(v1 == undefined)
                return this._storage[name];
        } else
            throw new Error('Unrecognized field name.');
    },

    exchange: function(v){
        /*
         * Dump an IDENTITY
         *
         * Usage:
         *  1) IDENTITY.exchange(); // dumps data in serialized form of this IDENTITY.
         *  2) IDENTITY.exchange('...'); // loads an IDENTITY from serialized data.
         */
        if(v == undefined){
            console.log('case 1');
        } else {
            console.log('case 2');
        }
    },

};

module.export = IDENTITY;

IDENTITY.val('description');
