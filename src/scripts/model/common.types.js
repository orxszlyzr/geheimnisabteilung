module.export = {
    object: function(t){
        // http://kevin.vanzonneveld.net
        // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Legaev Andrey
        // +   improved by: Michael White (http://getsprink.com)
        // *     example 1: is_object('23');
        // *     returns 1: false
        // *     example 2: is_object({foo: 'bar'});
        // *     returns 2: true
        // *     example 3: is_object(null);
        // *     returns 3: false
        if (Object.prototype.toString.call(t) === '[object Array]') {
        return false;
        }
        return t !== null && typeof t=== 'object';
    },

    arrayKV: function(t){
        if(!this.isObject(t)) return false;

        return true;
    },

    arrayKVItem: function(t){
    },

    integer: function(t){
        return (typeof t === 'number') && Math.floor(t) === t;
    },

    string: function(t){
        return (typeof t === 'string');
    },

    base32: function(t){
        if(this.string(t) === false) return false;
        //TODO check b32 encoded
        return true;
    },
}
